---------------
Plannig
---------------




- JP Task: What would the app look like if I envisioned it
    - Consumer Version 
        - allows user to compare prices for an area/region accross a range of stores
    - PriceIt [Name TBD]
        - allows team to submit new data for pricing changes 


- What do I expect from JP?
    - User data/privacy protections
    - Compensation

- What are my concerns?

- What are my motivations?

- How does this fit into a future with steph?

- What would I say to my future self to explain why I thought this was the best move at the time?