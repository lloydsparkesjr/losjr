public class Solution {
    public bool ContainsDuplicate(int[] nums) {
        HashSet<int> storeInts = new HashSet<int>();
        for(int i = 0; i < nums.Length; i++)
            if(storeInts.Add(nums[i]) == false)
                return true;
        return false;
    }
}

public class Solution {
    public bool ContainsDuplicate(int[] nums) {
        HashSet<int> hs = new HashSet<int>();
        foreach (var n in nums){
            if (!hs.Add(n)) return true;
        }
        return false;
    }
}

//less efficient but shows that a hashset has a Contains method
public class Solution {
    public bool ContainsDuplicate(int[] nums) {
        var copy = new HashSet<int>();
        for (var i = 0; i < nums.Length; i++) {
            if (copy.Contains(nums[i])) return true;
            else copy.Add(nums[i]);
        }
        return false;
    }
}