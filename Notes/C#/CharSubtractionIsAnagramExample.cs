public class Solution {
    public bool IsAnagram(string s, string t) {
        if (s.Length != t.Length) {
            return false;
        }

        int[] c = new int[26];

        for (int i = 0; i < s.Length; i++) {
            int spot1 = s[i] - 'a';
            int spot2 = t[i] - 'a';

            c[spot1]++;
            c[spot2]--;
        }

        for (int i = 0; i < c.Length; i++) {
            if (c[i] != 0) {
                return false;
            }
        }

        return true;
    }
}

//a little less efficient due to sorting but still fast
//shows how to convert string to a char array and sort the array
//20221224 - sort
public class Solution {
    public bool IsAnagram(string s, string t) {
        char[] sChar = s.ToArray();
        char[] tChar = t.ToArray();
        Array.Sort(sChar);
        Array.Sort(tChar);
        return new String(sChar).Equals(new String(tChar));
    }
}