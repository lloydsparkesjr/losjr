public class Solution {
    public IList<IList<string>> GroupAnagrams(string[] strs) {
        // Groupings can be saved in dictionary
        // Key must be string with new string(ch)
         var dict = new Dictionary<string, IList<string>>();
                foreach (string str in strs)
                {
                    var ch = str.ToCharArray();
                    Array.Sort(ch);
                    var key =  new string(ch);
                    if (dict.TryGetValue(key, out IList<string> group))
                    {
                        group.Add(str);
                    }
                    else
                    {
                        dict.Add(key, new List<string>() { str });
                    }
                }
                return dict.Values.ToList();
    }
}

//a little less efficient but still valid
//shows the add range function
public class Solution {
    public IList<IList<string>> GroupAnagrams(string[] strs) {
        string tmp = "";
        var dict = new Dictionary<string, IList<string>>();
        for( int i = 0; i < strs.Length; i++)
        {
            tmp = SortString(strs[i]);
            if(!dict.ContainsKey(tmp))
            {
                dict.Add(tmp, new List<string>{strs[i]});
            }
            else
            {
                dict[tmp].Add(strs[i]);
            }
        }
        var strings = new List<IList<string>>();
        strings.AddRange(dict.Values);
        return strings;
    }

    public static string SortString(string str)
    {
        var chars = str.ToCharArray();
        Array.Sort(chars);
        return new string(chars);
    }
}