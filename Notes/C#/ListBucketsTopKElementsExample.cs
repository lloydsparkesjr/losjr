public class Solution {
    public int[] TopKFrequent(int[] nums, int k) {
        var dict = new Dictionary<int, int>();
        var result = new List<int>();

        // count occurences of each number
        foreach (var num in nums)
        {
            if (dict.ContainsKey(num)) dict[num]++;
            else dict[num] = 1;
        }

        // put results into bucket (array of lists)
        var buckets = new List<int>[nums.Length+1];
        foreach (var d in dict)
        {
            if (buckets[d.Value] == null) buckets[d.Value] = new List<int>();
            buckets[d.Value].Add(d.Key);
        }

        // Get k top values from the bucket 
        var l = buckets.Length;
        var numberOfResults = 0;
        for (int i = l - 1; i >= 0; i--)
        {
            if (buckets[i] == null) continue;
            for (int j = 0; j < buckets[i].Count(); j++)
            {
                result.Add(buckets[i][j]);
                numberOfResults++;
            }
            if (numberOfResults == k) break;
        }
        return result.ToArray();
    }
}