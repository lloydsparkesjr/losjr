January 7th, 2023

who was dad
what did he mean to us
what kind of man was he
what did he stand for
what wouldn't he stand for
what did he teach us
what did he care about
what did he want for us

strong
honest
generous
wise
fun
firm
strict
wonderful
patient

[Intro]
Everyone has a father. It's one of the few things we all have in common. But not everyone has a DAD. Someone who loves you, cares for you, and genuinely wants the best for you. Each of us had a different relationship with him, and even when he was strict and gave us some "tough love" we came to realize it was always coming from his desire for us to have a better life, make better decisions, and avoid making the same mistakes that he made growing up. 
---

[Fishing]
There's an old proverb that says give a man fish, and he'll eat for a day, but teach a man to fish and he'll eat for a lifetime. Dad was the type of man that took a saying like this to heart. Not only did he teach all his children how to catch a fish,    clean a fish,   AND have a good time doing it    (just kidding, no one enjoys cleaning fish, I mean have you smelled them, come on).... Jokes aside, Fishing with Dad taught us how to be patient, how to be forgiving, how to respect each other, and our environment, and most importantly how to appreciate being present in the moment. So many good times and good memories were shared in doing this activity. I honestly never thought I'd treasure them so much. So for me personally, being able to say "I'm going fishing with Dad" is one of the things I'll miss most now that he's gone. 
---

[Meaning of family]
Dad taught us the meaning of family, and how important it is to regularly check in on and strengthen the bonds that exist between us. It didn't have to be much. Sometimes it's just a phone call, or running an errand together, or grabbing some spicy chicken fingers from Checkers. In hindsight, it seems really obvious but the invisible ties that hold a family together can become weak and brittle without regular check ins to see how you've been doing. Dad was really good at this kind of thing. He was a man who always made the time to be there when it counted. He put in the effort to show each of his kids that we were loved, appreciated, and that we were cared for.
---

[A Large Family]
It goes without saying that our family is pretty big. When I was a kid, and people would occasionally ask me how many brothers and sisters I had, I typically had to hold up both of my hands and just start counting them one by one on my fingers just to make sure I didn't leave anyone out. It might not be clear based on what can be seen on this stage but including two children by marriage, Dad was a father to eleven children. But there are probably more than a few who looked up to him as a father figure in their lives. His generosity and his genuine desire to be a positive and reliable influence in their lives was undeniable. 
---

[Jamaica Family]
Most of Dad's children live in Florida while his others live back in his hometown in Jamaica. One of the many things I will always be grateful for is how much effort dad put into making sure his kids here had good strong bonds with our family across the ocean. Dad made sure to regularly bring us to Jamaica on holidays and summer vacations for weeks at a time so that we could spend time with our grandparents, our aunts, uncles, cousins, siblings and eventually a niece whom we hadn't met yet. Our first trip without him will be tough, but he laid the groundwork so that we would still feel right at home in his absence, so I thank him for that. 
---

[Dad's gift]
They say blood is thicker than water. But what they don't say is that being related by blood, doesn't guarantee the bonds that produce, love, trust, and friendship will be formed. Being present, showing up, and showing that you care is a must to keep a family strong and for it to stay that way through the generations. Dad had a natural gift for this and he had the wisdom to recognize how important it was. He possessed the ability to bring people together, to understand them and to be understood by them. He was impressive without trying to be. He gave respect, and received in return. I loved him, I envied him, and I admired him. Lately I find myself saying that he was glue that kept us together. The tree trunk to the growing branches of our family. He really was the rock that held us down and made us feel supported. Although he left big shoes to fill, we are stronger because he was in our lives and we are forever blessed to have had him as our father. 
---

[Dad's resilience]
If someone said to you its just Mind over matter... you might think to yourself, it's easier said, than done. And you'd be right. For most people, choosing to change and keeping that commitment is one of the toughest things they'll ever do. Some how Dad was different in this aspect. When he put his mind to something, he followed through and he never looked back. In 2007 Dad quit smoking cigarettes, cold turkey. He went from being a daily smoker to living a smoke free life, over night. It was honestly one of the most impressive things I had ever seen him do. I remember being so proud of his decision, because my younger brother Boyd and I used to put in a lot of effort when we were kids to hide his packs of cigarettes from him because we wanted him to stop so badly. Dad recently mentioned to me that he believed that smoking would have killed him a long time ago if he hadn't chosen to stop when he did. I think his decision was just based on his desire to see his children grow up because he knew we still needed him. Obviously we still wish he was here and that we had more time with him, but I do admit that I'm thankful for the additional time we were gifted because he was brave enough to overcome his addiction for the sake of his health and for his future.  
---

[Dad is like the reggae music that you feel in your soul]
Dad got us hooked on reggae from a young age. It was the music that filled our home when we woke up, and when we went to sleep. It it played in the van as we drove with our fishing poles in the back and the wind blowing through our fingertips. It played at all hours outside through the speakers as all of dad's friends and some of our family came over to visit and hang out. To this day I can't hear some Bob Marley without getting a bit of nostalgia. But honestly I love it. I love the music, I love the feelings I get when I hear it. And I'm not sure if this makes sense, but our dad was just like that feeling you get when you hear a song that you don't just hear with your ears, but you feel it in your soul. It resonates with you, it empowers you. It helps you find a strength and motivation inside yourself that you may not have realized was there. It's special, it's unique, it's wonderful... and so was he. 
---

[Dad's toes]
So this next story is kind of funny, or at least I think it is. Most people probably didn't know this about our father, but he could do something that was both unusual but it was also kind of hilarious. When you're a kid, especially young little boys, you sometimes want to test your strength against your parents. You know just size em up a little bit, and see where you stand. And as you would expect kids typically realize their parents are often way stronger than they thought, or way faster than they appear, thinking back on the time dad beat all of us down the road in a foot race... but that's a different story. This story has to do with dads feet... more specifically his toes. I'm not sure where he learned this from but some how we stumbled on to the fact that dad has a pincer grip for anything that was unfortunate enough to land in between his big toe and his middle toe. And as kids, these unfortunate things that ended up locked in that grip, were our fingers or hands. Once he got us we'd have to beg and plead with him to let us go... only for us to try again immediately to see if we were quick enough to get in and out before he got a hold of us with those monsters. It hurt, like a lot, but some how it was also fun too, so it was both a game, and a trap for our younger siblings who hadn't yet  learned of the unspoken rule to stay away from dads feet. When I grew up I came to realize that I too has this mini superpower, if that's what you want to call it. I could pick things up with my toes, and playfully pinch my little nieces and nephews when they weren't expecting it. Some of my brothers also had this "gift" as well, and I really do find it funny that we got it from Dad. These days I don't call them pinches any more but I call them "toe hugs" it makes them sound less threatening. Because everyone should want a hug right.. even if it's from a persons toes. 
---

[Conclusion]
To wrap this up, if you're sitting in front of me now, or watching this from the live stream in Jamaica. You already know how amazing our father was. You've witnessed it first hand through shared experiences and fond memories. He was strong, honest, generous, and wise. He was a father, a brother, a husband, an uncle, and a friend to so many. He cannot be replaced and he will never be forgotten. Thank you dad for all that you've done, all that you were, all that you sacrificed and for all the courage and strength that you brought into this world. You were the real MVP and we will always cherish the time we got to spend together. Thank you. 