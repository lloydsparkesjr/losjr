Algo Trading Tips Notes
------------------------
- set test to exit after X number of bars, whether its up or down as a way of determining how an algorithm is performing

- Bollinger Test?
- RSI
- buy after X number of green candles 3-7 green
- sell after X number of red candles
- use 120 minute bars 
- Mean Reversion Techniques Might be useful

Risk Prevention 
- Daily Loss Limiters
- Next Trade Delay
- Taking Weekends off
- A High Volatility Kill Switch