//Goal is to generate a clickable url


//`https://mavin.io/search?q=${display}&bt=sold&cat=183454&sort=PricePlusShippingHighest`
//https://mavin.io/search?q= magneton+082+rocket &bt=sold&cat=183454&sort=PricePlusShippingHighest

//https://mavin.io/search?q=magneton+082+rocket&bt=sold&cat=183454&sort=PricePlusShippingHighest

const readline = require('readline');
// @ts-ignore
const Promise = require('bluebird');

const chalk = require('chalk');
const {
    green,
    yellow,
    grey
} = chalk;
const _ = require('lodash');


const R1 = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

menuPrompt();

////////////////////////////////////////////////////////////

async function menuPrompt() {
    console.log('Please provide the details for your Pokemon search. ')

    let sessionExpiredTimeout = setTimeout(() => {
        console.log('\n\nYour time to make a selection has expired.');
        exitApplication();
    }, 5 * 60 * 1000);

    R1.question('\nWhat would you like to do? ', async (option) => {
        clearTimeout(sessionExpiredTimeout);

        await getPokemonDetails(option);

        // waitFor(5000)
        menuPrompt();

    });

}

function exitApplication() {
    console.log('\nHave a great day! Goodbye.\n\n');
    process.exit();
}

async function getPokemonDetails(search) {
    return new Promise(async resolve => {
        const searchTokens = search.split(' '),
            res = [],
            lowCaseSearch = _.toLower(search);

        _.each(searchTokens, (searchToken, index) => {
            let lowerCaseSearchToken = _.toLower(searchToken);
            // let lowCaseLine = _.toLower(searchToken);

            // if (index === 0 && _.includes(lowCaseLine, lowCaseSearch)) {
            //     //pokemon number or name
            // }

            if (index === 0) { // pokemon name 
                res.push(lowerCaseSearchToken)
            } else {
                switch (lowerCaseSearchToken) {
                    case 'g':
                        res.push('gym')
                        break;
                    case 'i':
                        res.push('island')
                        break;
                    case 'ng':
                    case 'stars':
                        res.push('genesis')
                        break;
                    case 'f':
                        res.push('fossil')
                        break;
                    case '1':
                        res.push('1st edition')
                        break;
                    case '2':
                        res.push('set 2')
                        break;
                    case 'sw':
                        res.push('swirl')
                        break;
                    case 'p':
                        res.push('promo')
                        break;
                    case 'r':
                        res.push('rocket')
                        break;
                    case 'u':
                        res.push('unlimited')
                        break;
                    case 'j':
                        res.push('jungle')
                        break;
                    case 'jap':
                        res.push('japanese')
                        break;
                    case 'h':
                        res.push('holo')
                        break;
                    case 'b':
                        res.push('base')
                        break;
                    case 're':
                        res.push('reverse')
                        break;
                    // case '':
                    //     res.push('')
                    //     break;
                    // case '':
                    //     res.push('')
                    //     break;
                    default:
                        res.push(lowerCaseSearchToken)
                        break;
                }
            }

        })

        const display = res.join('+').replace(/  /g, ' ').replace(/ /g, '+'),
        url = `https://mavin.io/search?q=${display}&bt=sold&cat=183454&sort=PricePlusShippingHighest`
        console.log(url)
        resolve(null);
    })
}

function waitFor(ms) {
    return new Promise(r => setTimeout(r, ms));
}