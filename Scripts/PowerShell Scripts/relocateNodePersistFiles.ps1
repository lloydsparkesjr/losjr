
#region Limit files returned
# STEPS to move files in chunks 
# perserve pwd
$sourcePath = "C:\Projects\crypto-currency-autotrader\.node-persist_BAK\storage\ProductionMissingCopies"
$movedFilesPath = "C:\Projects\crypto-currency-autotrader\.node-persist_BAK\storage\ProductionMissingCopies_Moved"
$destPath = "C:\Projects\crypto-currency-autotrader\.node-persist\storage\Production"
$original_location = Get-Location
$iterationCount = 0

function New-Folder {
    param (
        [string]$path
    )

    $testpath = Test-Path -Path $path

    if (!$testpath) { 
        New-Item -ItemType Directory -Force -Path $path
    }
}

New-Folder -Path $sourcePath
New-Folder -Path $destPath
New-Folder -Path $movedFilesPath

function Move-Files {
    param (
        [int]$count
    )
    # navigate to correct folder\
    Set-Location $sourcePath
    
    # get all the files 
    $files = Get-ChildItem | Select-Object -First $count
    $numOfFiles = $files | Measure-Object | Select-Object Count
    
    # write-output "numOfFiles is $numOfFiles"
    # write-output "numOfFiles is $numOfFiles.Count"
    # write-output "count is $count"


    if ($numOfFiles.psobject.properties["Count"].value -eq 0) {
        Write-Output "No more files. Exiting Powershell script"
        Set-Location $original_location
        Exit
    } 

    # write-output "hasMoreFiles is $hasMoreFiles"
            
    Write-Host "Copying $numOfFiles files to destination folder."

    foreach ($file in $files) {
        $sourcefilepath = $sourcePath + "\" + $file.Name
        $destfilepath = $destPath + "\" + $file.Name
        $testpath = Test-Path -Path $destfilepath
    
        if (!$testpath) {
            Copy-Item $sourcefilepath $destfilepath
            
            # Write-Host "File has been copied to destination folder."
            
        }
    
        Move-Item -Path $sourcefilepath -Destination $movedFilesPath
        # Remove-Item -Path $sourcefilepath
    }

}

function Start-CryptoCat {
    $cryptoCatPath = "C:\Projects\crypto-currency-autotrader"

    Set-Location $cryptoCatPath
    npm run start redis-switch

    # Get-Process node | Sort-Object starttime | Select-Object -Last 1
    Write-Output "CryptoCat has exited." 

}

function Start-RelocProcess {
    Write-Host "Converting from Node Perisit to Redis. Iteration #" $iterationCount
    $iterationCount = $iterationCount + 1
    # Write-Output "iterationCount is now $iterationCount"


    # Move-Files -count 2500
    Move-Files -count 10000

    Start-CryptoCat

    # Write-Output "hasMoreFiles is now $hasMoreFiles"
    # if($hasMoreFiles -eq $true) {
    # Write-Output "It seems we do have more files based on the flag being set to TRUE."
        
    # if ($iterationCount -lt 100) {
        Write-Output "We can restart the loop now. "
        Start-RelocProcess
    # }
    # }

}

Start-RelocProcess


# navigate back to original folder
Set-Location $original_location

#endregion


#region my first PowerShell Script

# function Move-Files {
#     process { write-host $_ -ForegroundColor Green }
# }

#pwd
# Get-Location
#cd
# Set-Location c:\Projects
#ls
# Get-ChildItem

# write-output "im to sexy for my laptop"
#endregion


# if (condition) {
    
# }




# copying files example

# [String] $KfxComputers = "C:\temp\Kofax Apps\servers.txt"

# # This file contains the list of servers you want to copy files/folders to
# $computers = get-content -Path $KfxComputers

# # the folder you want to copy to the servers in the $computer variable
# $sourceRoot = @("\\wdevkofx110\Kofax Software\Oracle Clients", 
#             "\\wdevkofx110\Kofax Software\Kofax Capture 11")

# # the destination location you want the file/folder(s) to be copied to
# $destinationRoot = "C$\temp"

# foreach ($computer in $computers) {

# $testpath = Test-Path -Path \\$computer\$destinationRoot

# if (!$testpath) 
# {
#     Write-Host "creating folder and copying files..." -ForegroundColor green

#     New-Item -ItemType Directory -Force -Path "\\$computer\$destinationRoot"
#     copy-item -Path $sourceRoot -Recurse -Destination 
#     "\\$computer\$destinationRoot" -Container

# } else {
#         Write-Host "$computer\$destinationRoot folder already exists"
#        }

# }`