
#region Limit files returned
# STEPS to move files in chunks 
# perserve pwd
$allFilesPath = "C:\Projects\crypto-currency-autotrader\.node-persist_BAK\storage\Production"
$missingFilesPath = "C:\Projects\crypto-currency-autotrader\.node-persist_BAK\storage\ProductionMissingCopies"
$movedFilesPath = "C:\Projects\crypto-currency-autotrader\.node-persist_TOEMPTY\storage\Production_Moved"

$original_location = Get-Location


function New-Folder {
    param (
        [string]$path
    )

    $testpath = Test-Path -Path $path

    if (!$testpath) { 
        New-Item -ItemType Directory -Force -Path $path
    }
}

New-Folder -Path $allFilesPath
New-Folder -Path $missingFilesPath
New-Folder -Path $movedFilesPath


function Compare-Folders {
    #in correct folder
    Set-Location $allFilesPath

    $files = Get-ChildItem | Sort-Object -Property CreationTime -Descending

    foreach ($file in $files) {
        $actualFilepath = $allFilesPath + "\" + $file.psobject.properties["Name"].value
        $filepath = $movedFilesPath + "\" + $file.psobject.properties["Name"].value
        $destFilePath = $missingFilesPath + "\" + $file.psobject.properties["Name"].value 
        $result = (Test-Path -Path $filepath -PathType Leaf) -OR (Test-Path -Path $destFilePath -PathType Leaf)

        if ($result -eq $false) {
            Copy-Item -Path $actualFilepath -Destination $destFilePath
        }
    }



}

# Start-RelocProcess
Compare-Folders

# navigate back to original folder
Set-Location $original_location

#endregion
