
$original_location = Get-Location
$products = @(
    # 'ETH-USD',
    # 'BTC-USD',
    # 'LTC-USD',
    # 'XLM-USD',
    # 'BCH-USD'
    # 'ETC-USD',
    # 'REP-USD'
    # 'EOS-USD',
    # 'ZRX-USD',
    # 'NMR-USD',
    # 'YFI-USD',
    # 'XTZ-USD',
    # 'OMG-USD'
    # 'LRC-USD',
    # 'COMP-USD',
    # 'ALGO-USD',
    # 'SNX-USD',
    # 'KNC-USD',
    # 'ZEC-USD',
    # 'ATOM-USD',
    # 'AAVE-USD',
    # 'LINK-USD',
    # 'SUSHI-USD'
    # 'DASH-USD',
    # 'BNT-USD',
    # 'OXT-USD',
    # 'UNI-USD',
    # 'CGLD-USD',
    # 'FIL-USD',
    # 'GRT-USD',
    # 'MATIC-USD',
    # 'BAND-USD',
    # 'BAL-USD',
    # 'NU-USD'
    # 'REN-USD',
    # 'UMA-USD',
    # 'SKL-USD',
    ### 'ONE-INCH-USD',
    'ADA-USD',
    'ANKR-USD',
    'CRV-USD',
    'ENJ-USD',
    'MKR-USD',
    'NKN-USD',
    'OGN-USD',
    'STORJ-USD'
)

function Start-CryptoCatLoop {
    $cryptoCatPath = "C:\Projects\crypto-currency-autotrader"

    Set-Location $cryptoCatPath

    tsc 

    foreach ($product in $products) {
        Write-Host "Running CryptoCat for Product: " $product
        npm run start 1 $product
    }


    # Get-Process node | Sort-Object starttime | Select-Object -Last 1
    Write-Output "CryptoCat has exited." 

}

Start-CryptoCatLoop



# navigate back to original folder
Set-Location $original_location

#endregion


#region my first PowerShell Script

# function Move-Files {
#     process { write-host $_ -ForegroundColor Green }
# }

#pwd
# Get-Location
#cd
# Set-Location c:\Projects
#ls
# Get-ChildItem

# write-output "im to sexy for my laptop"
#endregion


# if (condition) {
    
# }




# copying files example

# [String] $KfxComputers = "C:\temp\Kofax Apps\servers.txt"

# # This file contains the list of servers you want to copy files/folders to
# $computers = get-content -Path $KfxComputers

# # the folder you want to copy to the servers in the $computer variable
# $sourceRoot = @("\\wdevkofx110\Kofax Software\Oracle Clients", 
#             "\\wdevkofx110\Kofax Software\Kofax Capture 11")

# # the destination location you want the file/folder(s) to be copied to
# $destinationRoot = "C$\temp"

# foreach ($computer in $computers) {

# $testpath = Test-Path -Path \\$computer\$destinationRoot

# if (!$testpath) 
# {
#     Write-Host "creating folder and copying files..." -ForegroundColor green

#     New-Item -ItemType Directory -Force -Path "\\$computer\$destinationRoot"
#     copy-item -Path $sourceRoot -Recurse -Destination 
#     "\\$computer\$destinationRoot" -Container

# } else {
#         Write-Host "$computer\$destinationRoot folder already exists"
#        }

# }`