class Person:
    '''this is a person class, used to practice making classes'''
    # name = ''
    def __init__(self, name):
        self.name = name
        
    def getName(self):
        return self.name
    


p1 = Person('lloyd sr')

print(Person.__doc__ == p1.__doc__)
print(p1.__doc__)
print(p1.getName() == p1.name)
print(p1.name) 
    