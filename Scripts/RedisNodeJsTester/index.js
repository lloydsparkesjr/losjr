const redis = require('redis')

const REDIS_PORT = process.env.REDIS_PORT || 6379;

const client = redis.createClient(REDIS_PORT);


// doSomething();
// newTest();
getHashTest();

// client.end()
////////////////////

client.on("error", function (err) {
    console.log("Error " + err);
});

////////////////////
function getHashTest() {
    client.hget('CryptoCat', 'Prod:LTC-USD:60:2019-03-02T04:12:00.000Z', (err, reply) => {
        console.log('hget has a reply for you sir', JSON.parse(reply))
    })
}
async function newTest() {
    client.get("name", (err, reply) =>{
        console.log(`err: ${err}`)
        console.log(`reply: ${reply}`)
    })
    const nom = await client.get("name")

    console.log(`nom: ${nom}`)
}

function doSomething() {
    // client.setex('thatWasEasy', 3600, 'becky') //same as set but with an expiration
    // client.set("string key", "string val", redis.print);

    client.hset("hash key22", "hashtest 1", "some value", (v) => {
        console.log("what is this: ", v)
        redis.print
    });
    
    // client.hset("hash key", "hashtest 1", "some value", redis.print);
    // client.hset("hash key", "hashtest 1", "some value", redis.print);
    // client.hset(["hash key", "hashtest 2", "some other value"], redis.print);


    // //loop hash keys by the name of the hash
    // client.hkeys("hash key", function (err, replies) {
    //     console.log(replies.length + " replies:");
    //     replies.forEach(function (reply, i) {
    //         console.log("    " + i + ": " + reply);
    //     });
    //     client.quit();
    // });

    
    exit();
}


function exit() {
    setTimeout(() => {
        process.exit()
    }, 5000)
}
