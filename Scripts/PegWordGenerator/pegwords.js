const readline = require('readline');
const fs = require('fs');
const {
    wrap
} = require('module');
const {
    argv
} = require('process');




async function generatePegWords() {
    return new Promise(resolve => {
        const filename = 'C:\\Projects\\LOSJR\\words.txt',
            rl = readline.createInterface({
                input: fs.createReadStream(filename),
                output: process.stdout,
                terminal: false
            }),
            reg = new RegExp(/[^sztdnmrljckgfvpb(sh|ch)]*/),
            wrap = (str) => new RegExp(`${reg.source}${str}${reg.source}`, 'gi'),
            keyHash = {
                0: wrap('(s|z){1,2}'),
                1: wrap('(t|d){1,2}'),
                2: wrap('n{1,2}'),
                3: wrap('m{1,2}'),
                4: wrap('r{1,2}'),
                5: wrap('l{1,2}'),
                6: wrap('(j|g|sh|ch)'),
                7: wrap('(g|c|k)'),
                8: wrap('(f|v){1,2}'),
                9: wrap('(p|b){1,2}'),
            },
            numToRegex = (val) => {
                let str = `${val}`,
                    expression = ''

                for (let x = 0; x < str.length; x++) {
                    expression += `${keyHash[Number(str[x])].source}`
                }

                return new RegExp(expression, 'g')
            }
        charToRegex = (val) => {
            let str = `${val}`,
                expression = ''

            for (let x = 0; x < str.length; x++) {
                expression += `${str[x]}.*`
            }

            return new RegExp(expression, 'g')
        }
        wordHash = (() => {
            z = {}
            for(let j = 0; j <= 1000; j++) {
                z[j] = [];
            }
            return z 
        })(),
        reHash = (() => {
            z = {}
            for(let j = 0; j <= 1000; j++) {
                z[j] = numToRegex(j);
            }
            return z 
        })()
        let words = []
        // let re = Number(argv[2]) ?
        //     numToRegex(argv[2]) :
        //     charToRegex(argv[2])


        console.log('Peg Words Started')

        rl.on('line', (line) => {

            for(let j = 0; j <= 1000; j++) {
                let m = line.match(reHash[j]);

                if (m && m[0] == line) {
                    wordHash[j].push(line);
                }
            }

            // let match = line.match(re)

            // if (match && match[0] == line) {
                
            //     words.push(line)
            //     // console.log(i, line)
            //     i++;
            // }
          
        })

        rl.on('close', () => {
            let output = fs.createWriteStream("output.txt")

            for(let j = 0; j <= 1000; j++) {
                wordHash[j].sort((a, b) => a.length - b.length)
                output.write(`${j}:  ${wordHash[j]} \n\n`)
            }

            output.end()

            // words.sort((a, b) => a.length - b.length)
            // console.log(words)
            console.log('All done parsing. ')
            resolve();
        });
    })
}


generatePegWords()
