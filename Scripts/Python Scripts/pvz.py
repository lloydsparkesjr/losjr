from copy import deepcopy


def shiftLeft(arr):
    el = arr[0]
    del arr[0]
    arr.append(el)
    return arr


def shiftRight(arr):
    arr.insert(0, arr.pop())
    return arr


def swap(arr):
    thirdPos = arr[2]
    fifthPos = arr[4]
    arr[2] = fifthPos
    arr[4] = thirdPos
    return arr

def isInOrder(arr):
    for x,y in zip(arr, range(len(arr))):
        if x != y:
            return False
            break
    return True

def sortBfs(arr):
    '''use bfs to sort the array to find the least number of steps to get to the solution'''
    
    print('sort BFS starting')
    isSorted = False
    l = [(arr, ['Start'])]
    
    i = 0
    while not isSorted:
        lTemp = []
        
        for array,path in l:
            i +=1
            
            if isInOrder(array):
                return print(f'Found it!! After {i} Quickest route is: ', [(i,j) for i,j in enumerate(path)])
            
            if path[-1] != 'S':
                pathCopy = deepcopy(path)
                pathCopy.append(('S'))
                lTemp.append((swap(array.copy()), pathCopy))
            
            if path[-1] != 'R':    
                pathCopy = deepcopy(path)
                pathCopy.append(('L'))
                lTemp.append((shiftLeft(array.copy()), pathCopy))
            
            if path[-1] != 'L':    
                pathCopy = deepcopy(path)
                pathCopy.append(('R'))
                lTemp.append((shiftRight(array.copy()), pathCopy))
        
        l = deepcopy(lTemp)
            
    
d = {
    'shovel': 0,
    'fence': 1,
    'flamingo': 2,
    'plant': 3,
    'glove': 4,
    'fork': 5,
    'boot': 6
}
    
if __name__ == '__main__':
    # arr = [0, 3, 2, 4, 6, 1, 5] 
    # arr = [0, 1, 2, 3, 4, 5, 6] 
    # print(swap(arr))
    # print(swap(arr))
    # print(shiftLeft(arr))
    # print(shiftRight(arr))
    # sort()
    # ['Shovel' 0 , 'Fence' 1, 'Flamingo' 2, 'Plant' 3, 'Glove' 4, 'Fork' 5, 'Boot' 6]
    
    
    
    
    #fence shovel plant glove fork boot flaminog
    # sortBfs([1,0, 3, 4, 5, 6, 2])
    # sortBfs([d[], d[], d[], d[], d[], d[], d[]])
    # sortBfs([d['fence'], d['shovel'], d['plant'], d['glove'], d['fork'], d['boot'], d['flamingo']])
    sortBfs([d['shovel'], d['fence'], d['flamingo'], d['plant'], d['glove'], d['fork'], d['boot']])