import pynput
import time
from pynput.keyboard import Key, Controller
from datetime import datetime, timedelta, timezone
from math import floor
from util import move_cursor, ts, gettimeelapsedbydiff
from sys import stdout
keyboard = Controller()


def stayawake(message='Refreshing', interval=60, prev=[]):
    starttime = datetime.today().timestamp()
    
    if len(prev) > 0:
        timeelapsed = gettimeelapsedbydiff(timedelta(seconds=datetime.today().timestamp() - prev[-1]))
        
        print(timeelapsed, 'since last session.\n\n')    

    try:
        while True:
            timeelapsed = gettimeelapsedbydiff(timedelta(seconds=datetime.today().timestamp() - starttime))
            stdout.write(message + ' ' + timeelapsed + '\r')
            keyboard.release(Key.ctrl)
            time.sleep(interval)
    except KeyboardInterrupt:
        timeelapsed = gettimeelapsedbydiff(timedelta(seconds=datetime.today().timestamp() - starttime))
        print('\nTime Elapsed: ', timeelapsed)
        ts(getonly=False)
    finally:
        prev.append(datetime.today().timestamp())
        
        
# stayawake(interval=3)