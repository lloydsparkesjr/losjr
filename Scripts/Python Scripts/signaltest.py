import signal
import threading
from time import sleep
mybaby = 'awesome'
TIMEOUT = 5  # number of seconds your want for timeout


def handler(signum, frame):
    "called when read times out"
    print('timeout!')
    raise


signal.signal(2, handler)


def signal_start():
    signal.raise_signal(signal.SIGINT)
    sleep(1)
    signal.raise_signal(0)


def input_timeout():
    try:
        print('You have 5 seconds to type in your stuff...')
        foo = input(': ')
        timer = threading.Timer(5.0, signal_start)
        timer.start()
        return foo
    except:
        # timeout
        return


def dosomethincrazy():
    global mybaby
    while True:
        print('oh snap! my baby is ', mybaby)
        sleep(3)


def refresh_loop():
    try:
        t1 = threading.Thread(target=dosomethincrazy)
        t1.start()
        return
    except:
        # timeout
        return

t.mybaby = 'gorgeous'


# s = input_timeout()
# disable the alarm after success
# signal.raise_signal(0)
# print('You typed', s)

refresh_loop()
