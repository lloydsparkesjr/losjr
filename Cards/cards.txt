strong
kind
caring
giving
genuine 
a great cook
a tough, but hands on mom


You have always been the best sister
I could have asked for. So on your birthday, 
I want to remind you that you are a great 
cook..... Nah I'm just playing, although 
your cooking is amazing. You are more than 
the sum of the stomachs you've filled and 
the tastebuds you've pleased. You are strong, 
caring, giving, and one of the most genuine 
people I know. You can do anything you put 
your mind to. I've always known that about 
you. I just wanted to remind you of how 
special you are. Never forget it. 

Happy Birthday Sis

- Lloyd  


Happy Belated Birthday 
To worlds best sister.
I hope it was as wonderful
as you are. 

- with love from Lucky

Taliah, We can't help but be 
so proud of you and all that you 
are. Your courage and your spirit, 
are more inspiring than any of us 
could have imagined. Keep following 
your heart and pursuing your passion.
If it's what you want, then it's what 
we want too. Keep your head up and 
never be afraid to ask for help.
 
Happy (Belated) Birthday to a niece any Uncle would be jealous to have.

- Your uncles Lloyd and Boyd




