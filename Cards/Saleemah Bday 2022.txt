resilient
persevere
strentgh
accepting
funny
considerate
generous
intelligent


Saleemah, we are blessed to have you in our lives. Through the good times and the bad we will always be here for you, as you have always been there for us. My sister, you are resilient, intelligent, generous, funny, and considerate. The way you are able to fill a room of people with warmth, love, and laughter is a gift that doesn't get acknowledged enough. Our lives are better because we have a sister like you. We love and appreciate you. We are forever grateful for all that you are, and all that you do. Just don't feel the need to do it all on your own. We are a family, and we will always be here to lighten the load when you need it.

Happy Birthday
To a sister who deserves it all and more. 
With Love,
Lloyd & Boyd
